// INPUT:
// Nhập đầu vào là số thực cho 5 giá trị: a , b, c, d, e .

// PROCESS:
// Cộng 5 giá trị của: a, b, c, d, e và chia cho 5

// OUTPUT:
// Gía trị là trung bình cộng của 5 số.

var a = 3.5;
var b = 4.6;
var c = 2.4;
var d = 3.2;
var e = 2.7;

var result = (a + b + c + d + e) / 5;
console.log("trung bình 5 số =", result);
