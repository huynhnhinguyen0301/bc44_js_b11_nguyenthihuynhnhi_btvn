// INPUT:
// nhập số Đô và giá quy đổi tiền Việt Nam

// PROCESS:
// Đô * vnđ

// OUTPUT:
// Thành tiền

var USD = 5;
var VND = 23500;

var result = USD * VND;
console.log("Thành tiền =", result);