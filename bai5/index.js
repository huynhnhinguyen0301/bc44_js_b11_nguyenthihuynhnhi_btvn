// INPUT:
// số tự nhiên gồm 2 số

// PROCESS:
// hàng chục / 10 (làm tròn)
// hàng đơn vị %10

// OUTPUT:
// cộng ký số lại với nhau

var a = 45;
var hangChuc;
var hangDonVi;
var result;

hangChuc = Math.floor(a / 10);
console.log(hangChuc);

hangDonVi = a % 10;
console.log(hangDonVi);

result = hangDonVi + hangChuc;
console.log(result);
