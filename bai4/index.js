// INPUT:
// lấy chiều dài và chiều rộng

// PROCESS:
// tính và xuất ra diện tích và chu vi của hình chữ nhật đó

// OUTPUT:
// chu vi và diện tích

var chieudai = 5;
var chieurong = 10;

var chuvi = (chieudai + chieurong) * 2;
console.log("chu vi = ", chuvi);

var dientich = chieudai * chieurong;
console.log("dien tich = ", dientich);
